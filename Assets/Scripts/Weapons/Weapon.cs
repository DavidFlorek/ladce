﻿using UnityEngine;
using System.Collections;

public enum FireType
{
	//pistol, sniper rifle
	SingleShot,
	//machinegun, assault rifle
	Automatic,
}

public enum WeaponType
{
	//pistol, machine gun...
	Raycast,
	//shotgun
	Shotgun,
	//rocket launcher, grenade launcher
	Projectile
}

public enum SecondaryType
{
	//no
	None,
	//burstfire secondary
	Burstfire,
	//zoom secondary
	Zoom,
	//projectile secondary
	//not implemented yet
	Projectile
}

[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour
{
	//type of the weapon
	public WeaponType weaponType;
	//type of the primary fire of weapon
	public FireType fireType;
	//type of the secondary mechanic of weapon
	public SecondaryType secondaryFireType;
	//type of effect to be drawn
	public EffectType effectType;

	//layermask for raycast
	public LayerMask layerMask;

	//delay between shots
	public float shotDelay;
	//timer, that checks for the delay
	[HideInInspector]public float shotDelayTimer;
	//delay between the use of secondary
	public float secondaryDelay;
	//timer, that checks for the secondary delay
	[HideInInspector]public float secondaryDelayTimer;

	//minimal and maximal damage to be done
	public float minDamage, maxDamage;
	//AnimationCurve that controlls the damage fallof, works on multiplication
	public AnimationCurve damageFallof;

	//number of pellets, only used with shotgun
	public int numberOfPellets;

	//spread of the gun, primary and secondary fire havedifferent spread
	public float primarySpread, secondarySpread;
	//spread of the gun, 0 is no spread
	private float spread;

	//range of the gun, only used with raycast and shotgun
	public float distance;

	//origin of the prohectile and the effect
	public Transform projectileOrigin;

	//animator of the gun
	private Animator anim;
	//Effects manager
	private FXManager effects;
	//fps controller
	private UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsController;
	//AmmoHolder of the player
	private AmmoHolder ah;
	//Inventory of the player
	private Inventory inventory;

	//did we shoot in this mouse click? for oneshot weapons
	private bool tapped = false;

	//How many bullets to fire in burstfire mode
	public int burstfireBullets;
	//Delay between burstfire shots
	public float burstfireDelay;
	//How many shots left in current burstfire
	private int burstfireLeft;

	//default FOV of camera
	private float defaultFOV;
	//FOV of zoomed camera
	public float zoomFOV;
	//Speed of camera zooming
	public float zoomTime;
	//Are we zoomed
	private bool zoomed = false;
	//Are we zooming (changing the FOV)
	private bool zooming = false;
	//Overlay drawn after zooming, for sniper rifles
	public GameObject zoomOverlay;
	//mouse sensitivity, normal is set, zoom is per weapon
	public float normalSensitivity = 2, zoomedSensitivity;

	//bullets per magazine
	public int magazineSize;
	//number of bullets currently in magazine
	[HideInInspector]public int currentAmmo;
	//type of ammo this weapon uses
	public AmmoType ammoType;
	//how much time it takes to reload
	public float reloadTime;

	//did we recently switch the gun?
	[HideInInspector]public bool switched = false;

	//muzzle particle effect to be played on shooting
	public ParticleSystem muzzle;
	//Object to be spawned, if we hit something with raycast gun
	public GameObject bulletHole;

	//gameobject with weapon camera
	private GameObject weaponCamera;
	//weapon camera
	private Camera weapCam;


	private RaycastHit hit;
	private Vector3 direction;
	private Vector3 inacuracy;

	public AudioSource audioSource;
	public AudioClip shootClip, reloadClip;
	public Vector2 shootPitchRange;

	void Start ()
	{
		primarySpread *= 0.01f;				//divide the spread by 100, to work with better numbers in editor
		secondarySpread *= 0.01f;

		spread = primarySpread;

		shotDelayTimer = secondaryDelayTimer = 0.25f;
		fpsController = Camera.main.GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ();
		fpsController.m_MouseLook.XSensitivity = normalSensitivity;
		fpsController.m_MouseLook.YSensitivity = normalSensitivity;
	}

	void Awake()
	{
		defaultFOV = Camera.main.fieldOfView;
		effects = FindObjectOfType<FXManager> ();
		anim = GetComponentInChildren<Animator> ();
		ah = GetComponentInParent<AmmoHolder> ();
		currentAmmo = magazineSize;
		inventory = GetComponentInParent<Inventory> ();
		weaponCamera = GameObject.FindWithTag ("Weapon Camera");
		weapCam = weaponCamera.GetComponent<Camera> ();
		//audioSource = GetComponent<AudioSource> ();
	}

	void Update ()
	{
		if (Time.timeScale == 0) {
			return;
		}

		shotDelayTimer -= Time.deltaTime;
		secondaryDelayTimer -= Time.deltaTime;

		if (currentAmmo <= 0 && shotDelayTimer < 0) {
			Reload ();
		}
		if (Input.GetAxisRaw ("Fire1") != 0 && currentAmmo > 0) {
			PrimaryFire ();	//do primary fire
		} else {
			tapped = false;	//reset the tap
		}

		if (Input.GetAxisRaw ("Fire2") != 0) {
			SecondaryFire ();	//do secondary
		}

		if (Input.GetAxisRaw ("Reload") != 0 && shotDelayTimer < 0 && currentAmmo < magazineSize) {
			Reload ();	//do secondary
		}

		if (zooming) {
			Zoom ();
		}

		if (Input.GetAxisRaw ("SwitchGun") != 0) {
			if (!switched && shotDelayTimer < 0) {
				inventory.SwitchGun ();
			}
		} else {
			switched = false;
		}
	}

	void PrimaryFire ()
	{
		if (shotDelayTimer < 0 || burstfireLeft > 0) {

			if (fireType == FireType.SingleShot && tapped == true) {
				return;
			}

			switch (weaponType) {
			case WeaponType.Raycast:
				{
					inacuracy = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range(-spread, spread);
					if (fpsController.m_IsCrouching) {
						inacuracy *= 0.5f;
					}
					direction = Camera.main.transform.forward.normalized + inacuracy;
					direction.Normalize ();

					if (Physics.Raycast (Camera.main.transform.position, direction, out hit, distance, layerMask.value)) {
						//Effects
						//Damage

						if (hit.transform.GetComponent<Rigidbody> () != null) {
							hit.transform.GetComponent<Rigidbody> ().AddForce (direction * 5, ForceMode.Impulse);
						}

						if (hit.transform.GetComponent<HitBox> () != null) {
							float eval = Vector3.Distance(Camera.main.transform.position, hit.point);
							float damage = Random.Range (minDamage, maxDamage) * damageFallof.Evaluate (eval);
							hit.transform.GetComponent<HitBox> ().DoDamage(damage, hit.point);
						}

						effects.RayCastBullet (projectileOrigin.position, hit.point, effectType);


						Quaternion hitRotation = Quaternion.FromToRotation(-Vector3.forward, hit.normal);
						GameObject bullet = (GameObject)Instantiate(bulletHole, hit.point, hitRotation);
						bullet.transform.parent = hit.transform;

					} else {
						effects.RayCastBullet (projectileOrigin.position, Camera.main.transform.position + direction * distance, effectType);
					}
					break;
				}
			case WeaponType.Shotgun:
				{
					//Effects
					//Damage

					for (int i = 0; i < numberOfPellets; i++) {
						inacuracy = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range(0, spread);
						direction = Camera.main.transform.forward.normalized + inacuracy;
						direction.Normalize ();

						if (Physics.Raycast (Camera.main.transform.position, direction, out hit, distance, layerMask.value)) {
							effects.RayCastBullet (projectileOrigin.position, hit.point, effectType);

							if (hit.transform.GetComponent<Rigidbody> () != null) {
								hit.transform.GetComponent<Rigidbody> ().AddForce (direction * 5, ForceMode.Impulse);
							}

							if (hit.transform.GetComponent<HitBox> () != null) {
								float eval = Vector3.Distance(Camera.main.transform.position, hit.point);
								float damage = Random.Range (minDamage, maxDamage) * damageFallof.Evaluate (eval);
								hit.transform.GetComponent<HitBox> ().DoDamage(damage, hit.point);
							}


							Quaternion hitRotation = Quaternion.FromToRotation(-Vector3.forward, hit.normal);
							GameObject bullet = (GameObject)Instantiate(bulletHole, hit.point, hitRotation);
							bullet.transform.parent = hit.transform;
						} else {
							effects.RayCastBullet (projectileOrigin.position, Camera.main.transform.position + direction * distance, effectType);
						}
					}

					break;
				}
			case WeaponType.Projectile:
				{
					//Effects
					inacuracy = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range(-spread, spread);
					direction = Camera.main.transform.forward.normalized + inacuracy;
					effects.Projectile (projectileOrigin.position, direction, effectType);

					break;
				}
			}
			if (burstfireLeft > 0) {
				burstfireLeft--;
			} else {
				spread = primarySpread;
				shotDelayTimer = shotDelay;
				secondaryDelayTimer = shotDelay;
				tapped = true;
			}
			anim.SetTrigger ("Shoot");
			currentAmmo--;
			if (shootClip != null) {
				audioSource.clip = shootClip;
				audioSource.pitch = Random.Range (shootPitchRange.x, shootPitchRange.y);
				audioSource.Play ();
			}

			if (muzzle != null && (!zoomed || zoomOverlay == null)) {
				muzzle.Play ();
			}
			if (currentAmmo <= 0) {
				burstfireLeft = 0;
				currentAmmo = 0;
			}
		}
	}

	void SecondaryFire ()
	{
		if (secondaryDelayTimer < 0) {
			spread = secondarySpread;
			switch (secondaryFireType) {
			case SecondaryType.Burstfire:
				{
					if (currentAmmo <= 0) {
						burstfireLeft = 0;
						currentAmmo = 0;
						return;
					}

					burstfireLeft = burstfireBullets;
					shotDelayTimer = (burstfireBullets + 1) * burstfireDelay;
					for (int i = 0; i < burstfireBullets; i++) {
						Invoke ("PrimaryFire", i * burstfireDelay);
					}
					break;
				}
			case SecondaryType.Zoom:
				{
					zoomed = !zoomed;
					zooming = true;
					break;
				}
			}
			secondaryDelayTimer = secondaryDelay;
		}
	}

	void Zoom ()
	{
		if (!zoomed) {
			Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, defaultFOV, zoomTime * Time.deltaTime);
			weapCam.fieldOfView = Camera.main.fieldOfView;
			if (zoomOverlay) {
				zoomOverlay.SetActive (false);
				weaponCamera.SetActive (true);
			}
			fpsController.m_MouseLook.XSensitivity = normalSensitivity;
			fpsController.m_MouseLook.YSensitivity = normalSensitivity;
			if (Mathf.Abs (Camera.main.fieldOfView - defaultFOV) < 0.5f) {
				zooming = false;
			}
		} else {
			Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, zoomFOV, zoomTime * Time.deltaTime);
			weapCam.fieldOfView = Camera.main.fieldOfView;
			if (zoomOverlay) {
				zoomOverlay.SetActive (true);
				weaponCamera.SetActive (false);
			}
			fpsController.m_MouseLook.XSensitivity = zoomedSensitivity;
			fpsController.m_MouseLook.YSensitivity = zoomedSensitivity;
			if (Mathf.Abs (Camera.main.fieldOfView - zoomFOV) < 0.5f) {
				zooming = false;
			}
		}
	}

	void Reload ()
	{
		int temp = ah.Reload (ammoType, magazineSize - currentAmmo);
		ZoomOut ();
		if (temp > 0) {
			shotDelayTimer = reloadTime;
			secondaryDelayTimer = reloadTime;
			currentAmmo += temp;
			anim.SetTrigger ("Reload");
			if (reloadClip != null) {
				audioSource.clip = reloadClip;
				audioSource.pitch = 1;
				audioSource.Play ();
			}
		} else {
			//inventory.SwitchGun ();
		}
	}

	public void ZoomOut()
	{
		zoomed = false;
		zooming = true;
		if (zoomOverlay) {
			zoomOverlay.SetActive (false);
			weaponCamera.SetActive (true);
		}
	}

	public void ZoomOutQuick()
	{
		zoomed = false;
		if (zoomOverlay) {
			zoomOverlay.SetActive (false);
			weaponCamera.SetActive (true);
		}
		Camera.main.fieldOfView = defaultFOV;
		weapCam.fieldOfView = defaultFOV;
	}

	public int GetCurrentAmmoInBag()
	{
		switch (ammoType) {
		case AmmoType.Pistol:
			return ah.currPistolAmmo;
		case AmmoType.AssaultRifle:
			return ah.currAssaultAmmo;
		case AmmoType.Grenade:
			return ah.currGrenades;
		case AmmoType.Rocket:
			return ah.currRockets;
		case AmmoType.Shotgun:
			return ah.currShotgunAmmo;
		case AmmoType.SniperRifle:
			return ah.currSniperAmmo;
		}
		return 0;
	}

	public void PickUp()
	{
		anim.SetTrigger ("Pick");
	}

}
