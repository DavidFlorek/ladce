﻿using UnityEngine;
using System.Collections;

public class WeaponCamera : MonoBehaviour 
{
	private Transform mainCamera;
	// Use this for initialization
	void Start () {
		mainCamera = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Slerp (transform.rotation, mainCamera.transform.rotation, 0.1f / Time.deltaTime);
	}
}
