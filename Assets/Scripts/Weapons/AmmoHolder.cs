﻿using UnityEngine;
using System.Collections;

public enum AmmoType{
	Pistol,
	AssaultRifle,
	SniperRifle,
	Shotgun,
	Grenade,
	Rocket
}

public class AmmoHolder : MonoBehaviour 
{
	public int maxPistolAmmo, maxAssaultAmmo, maxSniperAmmo, maxShotgunAmmo, maxGrenades, maxRockets;

	public int currPistolAmmo, currAssaultAmmo, currSniperAmmo, currShotgunAmmo, currGrenades, currRockets;

	public int Reload(AmmoType type, int amt)
	{
		int ret;
		switch (type) {
		case AmmoType.Pistol:
			{
				ret = ReloadAmt (currPistolAmmo, amt);
				currPistolAmmo -= ret;
				return ret;
			}

		case AmmoType.AssaultRifle:
			{
				ret = ReloadAmt (currAssaultAmmo, amt);
				currAssaultAmmo -= ret;
				return ret;
			}

		case AmmoType.SniperRifle:
			{
				ret = ReloadAmt (currSniperAmmo, amt);
				currSniperAmmo -= ret;
				return ret;
			}

		case AmmoType.Shotgun:
			{
				ret = ReloadAmt (currShotgunAmmo, amt);
				currShotgunAmmo -= ret;
				return ret;
			}

		case AmmoType.Grenade:
			{
				ret = ReloadAmt (currGrenades, amt);
				currGrenades -= ret;
				return ret;
			}

		case AmmoType.Rocket:
			{
				ret = ReloadAmt (currRockets, amt);
				currRockets -= ret;
				return ret;
			}
		}
		return 0;
	}

	public bool GiveAmmo(AmmoType type, int amt){
		switch (type) {
		case AmmoType.Pistol:
			{
				if (currPistolAmmo >= maxPistolAmmo) {
					return false;
				}
				currPistolAmmo += amt;
				if (currPistolAmmo > maxPistolAmmo) {
					currPistolAmmo = maxPistolAmmo;
				}
				return true;
			}

		case AmmoType.AssaultRifle:
			{
				if (currAssaultAmmo >= maxAssaultAmmo) {
					return false;
				}
				currAssaultAmmo += amt;
				if (currAssaultAmmo > maxAssaultAmmo) {
					currAssaultAmmo = maxAssaultAmmo;
				}
				return true;
			}

		case AmmoType.SniperRifle:
			{
				if (currSniperAmmo >= maxSniperAmmo) {
					return false;
				}
				currSniperAmmo += amt;
				if (currSniperAmmo > maxSniperAmmo) {
					currSniperAmmo = maxSniperAmmo;
				}
				return true;
			}

		case AmmoType.Shotgun:
			{
				if (currShotgunAmmo >= maxShotgunAmmo) {
					return false;
				}
				currShotgunAmmo+= amt;
				if (currShotgunAmmo > maxShotgunAmmo) {
					currShotgunAmmo = maxShotgunAmmo;
				}
				return true;
			}

		case AmmoType.Grenade:
			{
				if (currGrenades >= maxGrenades) {
					return false;
				}
				currGrenades += amt;
				if (currGrenades > maxGrenades) {
					currGrenades = maxGrenades;
				}
				return true;
			}

		case AmmoType.Rocket:
			{
				if (currRockets >= maxRockets) {
					return false;
				}
				currRockets += amt;
				if (currRockets > maxRockets) {
					currRockets = maxRockets;
				}
				return true;
			}
		}
		return false;
	}

	private int ReloadAmt(int curr, int amt){
		if (curr > amt) {
			return amt;
		} else {
			return curr;
		}
	}
}
