﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
	public GameObject[] guns;
	public string[] names;
	public int activeGun, sidearm;
	public GameObject[] dropedGuns;

	private Weapon[] weapons;

	private RaycastHit changeGunHit;
	public float distanceToChangeGun;
	public LayerMask layerMask;

	public Text tempAmmoText;
	public Text switchText;

	void Start ()
	{
		weapons = new Weapon[guns.Length];
		for (int i = 0; i < guns.Length; i++) {
			weapons [i] = guns [i].GetComponent<Weapon> ();
			guns [i].SetActive (false);
		}
		EquipGun (activeGun);
	}

	public void SwitchGun ()
	{
		HideGun (activeGun);
		EquipGun (sidearm);
		int temp = activeGun;
		activeGun = sidearm;
		sidearm = temp;
		weapons [activeGun].switched = true;

	}

	public void EquipGun (int index)
	{
		guns [index].SetActive (true);
		weapons [index].PickUp ();
		weapons [index].shotDelayTimer = 0.75f;
		weapons [index].secondaryDelayTimer = 0.75f;
		weapons [index].ZoomOut ();
	}

	public void HideGun (int index)
	{
		weapons [index].ZoomOutQuick ();
		guns [index].SetActive (false);
	}

	public void ChangeGun (int index)
	{
		weapons [activeGun].ZoomOutQuick ();
		HideGun (activeGun);
		EquipGun (index);
		GameObject dg = (GameObject)Instantiate (dropedGuns [activeGun], transform.position, transform.rotation);
		dg.GetComponent<DropedWeapon> ().ammo = weapons [activeGun].currentAmmo;
		activeGun = index;
	}

	void FixedUpdate ()
	{
		switchText.text = "";
		tempAmmoText.text = weapons [activeGun].currentAmmo + " | " + weapons [activeGun].GetCurrentAmmoInBag ();
		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.forward, out changeGunHit, distanceToChangeGun, layerMask)) {
			if (changeGunHit.transform.GetComponent<DropedWeapon> () != null) {
				DropedWeapon dw = changeGunHit.transform.GetComponent<DropedWeapon> ();
				if (dw.index != sidearm) {
					switchText.text = "Switch " + names [activeGun] + " for " + names [dw.index];
					if (Input.GetAxisRaw ("ChangeGun") != 0 && weapons [activeGun].shotDelayTimer < 0) {
						ChangeGun (changeGunHit.transform.GetComponent<DropedWeapon> ().index);
						weapons [activeGun].currentAmmo = dw.ammo;
						Destroy (changeGunHit.transform.gameObject);
					}
				}
			}
		}
	}

}