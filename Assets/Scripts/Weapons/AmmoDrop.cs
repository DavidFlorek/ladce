﻿using UnityEngine;
using System.Collections;

public class AmmoDrop : MonoBehaviour 
{
	public AmmoType ammoType;
	public int amt;
	public GameObject audioGameObject;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			if (other.GetComponent<AmmoHolder> ().GiveAmmo (ammoType, amt)) {
				Instantiate (audioGameObject, transform.position, transform.rotation);
				Destroy (transform.parent.gameObject);
			}
		}
	}
}
