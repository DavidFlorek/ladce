﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{
	public float speed;
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (transform.forward * speed * Time.deltaTime, Space.World);
	}
}
