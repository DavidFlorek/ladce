﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour
{
	public float force;
	public float timer;

	public float range;
	public float explosionForce;

	public float minDamage, maxDamage, hitDamage;

	public GameObject explosionPrefab;

	void Start ()
	{
		GetComponent<Rigidbody> ().AddForce (transform.forward * force + Vector3.up * force * 0.33f, ForceMode.Impulse);
	}

	void OnCollisionEnter(Collision collision) 
	{
		if (collision.gameObject.GetComponent<HitBox> () != null) {
			collision.gameObject.GetComponent<HitBox> ().DoDamage (hitDamage, transform.position);
		}
	}

	void Update ()
	{
		timer -= Time.deltaTime;
		if (timer < 0) {
			Collider[] col = Physics.OverlapSphere (transform.position, range);
			foreach (Collider c in col) {
				if (c.GetComponentInParent<Rigidbody> () != null) {
					c.GetComponentInParent<Rigidbody> ().AddExplosionForce (explosionForce, transform.position - Vector3.up, range);
				}

				if (c.GetComponentInParent<Health> () != null) {
					int damage = (int)(Random.Range (minDamage, maxDamage) * (1 - Vector3.Distance (transform.position, c.transform.position) / range));
					c.GetComponentInParent<Health> ().DealDamage (damage, transform.position);
				}
			}
			GameObject go = (GameObject)Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			go.GetComponent<AudioSource> ().pitch = Random.Range (0.5f, 1.75f);
			Destroy (gameObject);
		}
	}
}
