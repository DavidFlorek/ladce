﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class LRMover : MonoBehaviour 
{	
	public Vector3 origin, destination;

	public float speed, size;

	private float alpha = 0;
	private float timer, time;
	private Vector3 direction;

	private LineRenderer lr;

	private Vector3 position;

	void Start () {
		lr = GetComponent<LineRenderer> ();

		direction = destination - origin;
		time = direction.magnitude / speed;
		timer = time;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer < 0) {
			Destroy (gameObject);
		}
		alpha = 1 - timer / time;
		position = origin + alpha * direction;
		lr.SetPositions (new Vector3[] {position, position + size * direction.normalized });
		timer -= Time.deltaTime;
	}
}
