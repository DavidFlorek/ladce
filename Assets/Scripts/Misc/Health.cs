﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum DeathType
{
	Ragdoll,
	Player,
	Glass,
	Monitor
}

public class Health : MonoBehaviour 
{
	public int maxHealth;
	public int currentHealth;
	public DeathType deathType;

	public GameObject ragdoll;
	public GameObject gun;
	public GameObject ammoBox;
	[Range(0f, 1f)]public float chance;
	public GameObject brokenGlass;
	public int materialIndex;
	public Material material;

	public Text healthText;

	private BotAI botAI;

	void Start()
	{
		botAI = GetComponent<BotAI> ();
		if (healthText != null) {
			healthText.text = "+" + currentHealth;
		}
	}

	public bool Heal(int amt)
	{
		if (currentHealth >= maxHealth) {
			return false;
		} else {
			currentHealth += amt;
			if (currentHealth > maxHealth) {
				currentHealth = maxHealth;
			}
			if (healthText != null) {
				healthText.text = "+" + currentHealth;
			}
			return true;
		}
	}

	public void DealDamage(int amt, Vector3 point)
	{
		if (currentHealth > 0) {
			currentHealth -= amt;

			if (currentHealth <= 0) {
				Die (point);
			}

			if (botAI != null) {
				botAI.ChangeCover ();
				botAI.AlertFriends (15f);
			}
			if (healthText != null) {
				healthText.text = "+" + currentHealth;
			}
		}
	}

	private void Die(Vector3 point)
	{
		switch (deathType) {
		case DeathType.Ragdoll:
			GameObject rag = (GameObject)Instantiate (ragdoll, transform.position, transform.rotation);
			Rigidbody[] r = rag.GetComponentsInChildren<Rigidbody> ();
			foreach (Rigidbody rb in r) {
				rb.AddExplosionForce (25f, point, 0.4f, 0, ForceMode.Impulse);
			}
			if (gun != null) {
				GameObject g = (GameObject)Instantiate (gun, transform.position + Vector3.up, transform.rotation);
				g.GetComponent<DropedWeapon> ().ammo = Random.Range (0, g.GetComponent<DropedWeapon> ().ammo);
			}
			if (ammoBox != null && Random.Range (0f, 1f) < chance) {
				Instantiate (ammoBox, transform.position + 1.5f * Vector3.up, transform.rotation);
			}

			Destroy (gameObject);
			break;
		case DeathType.Player:
			SceneManager.LoadScene ("Menu");
			break;
		case DeathType.Glass:
			GameObject glass = (GameObject)Instantiate (brokenGlass, transform.position, transform.rotation);
			glass.transform.localScale = transform.localScale;
			Destroy (gameObject);
			break;
		case DeathType.Monitor:
			GetComponent<AudioSource> ().Play ();
			Material[] materials = GetComponentInChildren<MeshRenderer> ().materials;
			materials [materialIndex] = material;
			GetComponentInChildren<MeshRenderer> ().materials = materials;
			Destroy (GetComponentInChildren<HitBox> ());
			Destroy(GetComponent<Health>());
			Destroy(GetComponent<BillboardScroller>());
			break;
		}
	}

	void OnDestroy()
	{
		if (botAI != null) {
			botAI.UpdateEnemies ();
		}
	}
}
