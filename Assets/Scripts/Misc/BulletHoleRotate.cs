﻿using UnityEngine;
using System.Collections;

public class BulletHoleRotate : MonoBehaviour 
{
	public Vector3 range;

	void Start()
	{
		transform.Rotate (new Vector3 (Random.Range(-range.x, range.x), Random.Range(-range.y, range.y), Random.Range(-range.z, range.z)));
	}
}
