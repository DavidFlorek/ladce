﻿using UnityEngine;
using System.Collections;

public class BillboardScroller : MonoBehaviour 
{
	public int materialID;
	public Vector2 speed;

	private Material mat;

	void Start()
	{
		mat = GetComponent<MeshRenderer> ().materials [materialID];
	}

	void Update()
	{
		mat.mainTextureOffset += speed * Time.deltaTime;
	}

}
