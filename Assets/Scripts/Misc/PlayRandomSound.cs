﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayRandomSound : MonoBehaviour 
{
	public bool looping;
	public AudioClip[] clips;

	private AudioSource source;
	// Use this for initialization
	void Awake () 
	{
		source = GetComponent<AudioSource> ();
		source.clip = clips [Random.Range (0, clips.Length)];
		source.Play ();
	}

	void Update()
	{
		if (looping) {
			if (!source.isPlaying) {
				source.clip = clips [Random.Range (0, clips.Length)];
				source.Play ();
			}
		}
	}
}
