﻿using UnityEngine;
using System.Collections;

public class ChangeAudioVolume : MonoBehaviour 
{
	public AudioSource source;
	[Range(0f, 1f)]public float volume;

	void OnDestroy () 
	{
		if (source != null) {
			source.volume = volume;
		}
	}
}
