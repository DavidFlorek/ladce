﻿using UnityEngine;
using System.Collections;

public class SetMecanimSpeed : MonoBehaviour 
{
	private Animator anim;

	private Vector3 lastPosition;

	void Start()
	{
		anim = GetComponent<Animator> ();
		lastPosition = transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {
		anim.SetFloat ("Speed", (transform.position - lastPosition).magnitude / Time.deltaTime);
		lastPosition = transform.position;
	}
}
