﻿using UnityEngine;
using System.Collections;

public class CharacterAudio : MonoBehaviour 
{
	public AudioSource audioSource;
	public Vector2 pitchRange;
	public AudioClip[] reloading, takingFire;

	void Start()
	{
		audioSource.pitch = Random.Range (pitchRange.x, pitchRange.y);
	}

	public void PlayReload(float chance)
	{
		if (!audioSource.isPlaying && Random.Range (0f, 1f) < chance) {
			audioSource.clip = reloading [Random.Range (0, reloading.Length)];
			audioSource.Play ();
		}
	}

	public void PlayTakingFire(float chance)
	{
		if (!audioSource.isPlaying && Random.Range (0f, 1f) < chance) {
			audioSource.clip = takingFire [Random.Range (0, takingFire.Length)];
			audioSource.Play ();
		}
	}

}
