﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour 
{
	private Health health;
	public float damageMultyplier = 1;

	public GameObject particle;

	void Start()
	{
		health = GetComponent<Health> ();
		if (health == null) {
			health = GetComponentInParent<Health> ();
		}
	}

	public void DoDamage(float amt, Vector3 point)
	{
		health.DealDamage ((int)(amt * damageMultyplier), point);
		if (particle != null) {
			Instantiate (particle, point, Quaternion.identity);
		}
	}
}
