﻿using UnityEngine;
using System.Collections;

public class Medkit : MonoBehaviour 
{
	public int amt;
	public GameObject audioGameObject;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			if (other.GetComponent<Health> ().Heal (amt)) {
				Instantiate (audioGameObject, transform.position, transform.rotation);
				Destroy (gameObject);
			}
		}
	}
}
