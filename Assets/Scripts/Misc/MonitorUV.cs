﻿using UnityEngine;
using System.Collections;

public class MonitorUV : MonoBehaviour 
{
	public int index;
	public Vector2 minBounds, maxBounds;
	public Vector2 tiling = Vector2.one;

	// Use this for initialization
	void Start () {
		GetComponent<MeshRenderer> ().materials [index].mainTextureScale = tiling;
		GetComponent<MeshRenderer> ().materials [index].mainTextureOffset = new Vector2 (Random.Range (minBounds.x, maxBounds.x), Random.Range (minBounds.y, maxBounds.y));
	}
}
