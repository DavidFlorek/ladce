﻿using UnityEngine;
using System.Collections;

public enum EffectType
{
	SapperBullet,
	HornetBullet,
	RifleBullet,
	RaverPellet,
	ShotgunPellet,
	Rocket,
	Grenade,
	None,
	RaverPelletStatic
}

public class FXManager : MonoBehaviour
{
	//bullet prefabs
	public GameObject riffleBulletPrefab, shotgunPelletPrefab, sapperBullet, hornetBullet, raverPellet, raverPelletStatic;

	//projectile prefabs
	public GameObject rocketProjectilePrefab, grenadeProjectilePrefab;

	private GameObject bulletTracers;
	private GameObject projectiles;

	void Start ()
	{
		bulletTracers = new GameObject ("Bullet Tracers");
		projectiles = new GameObject ("Projectiles");
	}

	public void RayCastBullet (Vector3 origin, Vector3 destination, EffectType type)
	{
		if (Vector3.Distance (origin, destination) < 1f) {
			return;
		}
		GameObject bullet = null;
		switch (type) {
		case EffectType.RifleBullet:
			bullet = (GameObject)Instantiate (riffleBulletPrefab);
			bullet.transform.parent = bulletTracers.transform;
			bullet.GetComponent<LineRenderer> ().SetPositions (new Vector3[] { origin, origin });
			bullet.GetComponent<LRMover> ().origin = origin;
			bullet.GetComponent<LRMover> ().destination = destination;
			break;
		case EffectType.RaverPellet:
			bullet = (GameObject)Instantiate (raverPellet);
			bullet.transform.parent = bulletTracers.transform;
			bullet.GetComponent<LineRenderer> ().SetPositions (new Vector3[] { origin, origin });
			bullet.GetComponent<LRMover> ().origin = origin;
			bullet.GetComponent<LRMover> ().destination = destination;
			break;
		case EffectType.RaverPelletStatic:
			bullet = (GameObject)Instantiate (raverPelletStatic);
			bullet.transform.parent = bulletTracers.transform;
			bullet.GetComponent<LineRenderer> ().SetPositions (new Vector3[] { origin, destination });
			break;
		case EffectType.SapperBullet:
			bullet = (GameObject)Instantiate (sapperBullet);
			bullet.transform.parent = bulletTracers.transform;
			bullet.GetComponent<LineRenderer> ().SetPositions (new Vector3[] { origin, origin });
			bullet.GetComponent<LRMover> ().origin = origin;
			bullet.GetComponent<LRMover> ().destination = destination;
			break;
		case EffectType.HornetBullet:
			bullet = (GameObject)Instantiate (hornetBullet);
			bullet.GetComponent<LineRenderer> ().SetPositions (new Vector3[] { origin, destination });
			break;
		}

	}

	public void Projectile (Vector3 origin, Vector3 direction,  EffectType type)
	{
		GameObject projectile = null;
		switch (type) {
		case EffectType.Rocket:
			projectile = (GameObject)Instantiate (rocketProjectilePrefab, origin, Quaternion.LookRotation (direction));
			break;
		case EffectType.Grenade:
			projectile = (GameObject)Instantiate (grenadeProjectilePrefab, origin, Quaternion.LookRotation (direction));
			break;
		}
		projectile.transform.parent = projectiles.transform;
	}
}
