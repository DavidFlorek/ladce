﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AIState
{
	Idle,
	Patrol,
	ChangingCover,
	Covering,
	Seaching
}

[RequireComponent(typeof(AudioSource))]
public class BotAI : MonoBehaviour
{
	private AIState state = AIState.Idle;

	private NavMeshAgent agent;
	private Animator animator;
	private FXManager fxManager;

	private List<Vector3> pointsOfInterest;
	private List<Vector3> covers;
	private List<Transform> coverTransforms;
	private int currentCover = -1;

	private Vector3 target;
	private Vector3 lastEnenmyPosition;
	private float stopDistance = 1f;

	private Transform[] enemies;
	private int enemyIndex;
	private BotAI[] friends;

	public bool isFriend;
	public float fov;
	public float viewDistance;
	public Transform eyes;
	public LayerMask layermask;

	//TIMERS
	private float idleTimer, rotateTimer, searchTimer, coveringTimer, reloadTimer, shotTimer;

	//TIMER VALUES
	public Vector2 idleTimerBounds, rotateTimerBounds, searchTimerBounds, coveringTimerBunds;

	//SPEEDS
	public float patrolSpeed, coveringSpeed, searchingSpeed;

	//GUN STUFF
	public Transform projectileOrigin;
	public EffectType effectType;
	public int maxAmmo;
	private int currentAmmo;
	public float shotDelay;
	public float reloadTime;
	public float spread;
	public float accuracy;

	private Vector3 direction;
	private Vector3 inacuracy;
	private Vector3 spreadVector;

	public int numberOfPellets;
	public float distance;
	public float minDamage, maxDamage;

	private RaycastHit hit;

	public LayerMask shotLayermask;

	public AudioSource gunAudioSource;
	public AudioClip shootClip;
	public AudioClip reloadClip;
	public Vector2 shootPitchRange;
	private CharacterAudio charAudio;

	// Use this for initialization
	void Awake ()
	{
		animator = GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
		fxManager = FindObjectOfType<FXManager> ();
		charAudio = GetComponent<CharacterAudio> ();


		NavMeshPath path = new NavMeshPath();
		PointOfInterest[] poi = FindObjectsOfType<PointOfInterest> ();
		pointsOfInterest = new List<Vector3> ();
		for (int i = 0; i < poi.Length; i++) {
			Vector3 p = poi [i].GetPosition ();
			agent.CalculatePath (p, path);
			if (path.status == NavMeshPathStatus.PathComplete) {
				pointsOfInterest.Add (p);
			}

		}

		Cover[] cov = FindObjectsOfType<Cover> ();
		covers = new List<Vector3> ();
		coverTransforms = new List<Transform> ();
		for (int i = 0; i < cov.Length; i++) {
			Vector3 c = cov [i].GetPosition ();
			agent.CalculatePath (c, path);
			if (path.status == NavMeshPathStatus.PathComplete) {
				covers.Add (c);
				coverTransforms.Add (cov[i].transform);
			}
		}

		GetEnemies ();

		idleTimer = Random.Range (idleTimerBounds.x, idleTimerBounds.y);
		rotateTimer = Random.Range (rotateTimerBounds.x, rotateTimerBounds.y);
		agent.speed = 0;
		enemyIndex = -1;

		currentAmmo = maxAmmo;
	}

	void Update ()
	{
		reloadTimer -= Time.deltaTime;
		shotTimer -= Time.deltaTime;
		switch (state) {
		case AIState.Idle:
			idleTimer -= Time.deltaTime;
			rotateTimer -= Time.deltaTime;

			if (idleTimer < 0) {
				StartPatroling ();
			}

			/*
			if (rotateTimer < 0) {
				transform.Rotate (0, Random.Range (-20f, 20f), 0);
				rotateTimer = Random.Range (rotateTimerBounds.x, rotateTimerBounds.y);
			}*/
			break;

		case AIState.Patrol:
			if (agent.remainingDistance < stopDistance) {
				idleTimer = Random.Range (idleTimerBounds.x, idleTimerBounds.y);
				rotateTimer = Random.Range (rotateTimerBounds.x, rotateTimerBounds.y);
				state = AIState.Idle;
				agent.speed = 0;
			}
			break;
		
		case AIState.ChangingCover:
			if (shotTimer < 0) {
				agent.speed = coveringSpeed;
			} else {
				agent.speed = 0;
			}

			if (agent.remainingDistance < stopDistance) {
				state = AIState.Covering;
				agent.speed = 0;
				coveringTimer = Random.Range (coveringTimerBunds.x, coveringTimerBunds.y);
			}
			break;

		case AIState.Covering:
			coveringTimer -= Time.deltaTime;
			/*
			if (rotateTimer < 0) {
				transform.Rotate (0, Random.Range (-45f, 45f), 0);
				rotateTimer = Random.Range (rotateTimerBounds.x, rotateTimerBounds.y);
			}*/
			if (enemyIndex >= 0) {

			} else {
				transform.rotation = Quaternion.Slerp (transform.rotation, coverTransforms [currentCover].rotation, 0.3f);
			}

			if (coveringTimer < 0) {
				target = lastEnenmyPosition;
				agent.destination = target;
				searchTimer = Random.Range (searchTimerBounds.x, searchTimerBounds.y);
				state = AIState.Seaching;
			}
			break;

		case AIState.Seaching:
			if (shotTimer < 0) {
				agent.speed = searchingSpeed;
			} else {
				agent.speed = 0;
			}
			searchTimer -= Time.deltaTime;
			if (searchTimer < 0) {
				StartPatroling ();
			}
			break;
		}

		GetClosestVisibleTarget ();

		if (enemyIndex >= 0) {
			if (Vector3.Distance (enemies [enemyIndex].position, transform.position) < distance - stopDistance && shotTimer < 0 && reloadTimer < 0) {
				agent.speed = 0;
				Shoot ();
			}
			lastEnenmyPosition = enemies [enemyIndex].position;
		}

		if (enemyIndex >= 0 && (state == AIState.Idle || state == AIState.Patrol || state == AIState.Seaching) && state != AIState.ChangingCover) {
			if (shotTimer < 0) {
				shotTimer = 1f;
			} else {
				shotTimer += 1f;
			}
			ChangeCover ();
			AlertFriends (20f);
		}

		animator.SetFloat ("Speed", agent.velocity.magnitude);
	}

	void Shoot ()
	{
		transform.LookAt (enemies [enemyIndex].position);
		transform.rotation = Quaternion.Euler (0, transform.rotation.eulerAngles.y, 0);
		inacuracy = new Vector3 (Random.Range (-accuracy, accuracy), Random.Range (-accuracy, accuracy), Random.Range (-accuracy, accuracy)).normalized * Random.Range (0, accuracy);
		direction = ((enemies [enemyIndex].position + 0.7f * Vector3.up) - projectileOrigin.position).normalized + inacuracy;
		switch (effectType) {
		case EffectType.Grenade:
			spreadVector = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range (0, spread);
			direction += spreadVector;
			direction.Normalize ();
			fxManager.Projectile (projectileOrigin.position, direction, effectType);
			break;
		case EffectType.RaverPellet:
		case EffectType.RaverPelletStatic:

			for (int i = 0; i < numberOfPellets; i++) {
				spreadVector = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range (0, spread);
				direction += spreadVector;
				direction.Normalize ();

				if (Physics.Raycast (projectileOrigin.position, direction, out hit, distance, shotLayermask.value)) {
					fxManager.RayCastBullet (projectileOrigin.position, hit.point, effectType);

					if (hit.transform.GetComponent<Rigidbody> () != null) {
						hit.transform.GetComponent<Rigidbody> ().AddForce (direction * 5, ForceMode.Impulse);
					}

					if (hit.transform.GetComponent<HitBox> () != null) {
						hit.transform.GetComponent<HitBox> ().DoDamage (Random.Range (minDamage, maxDamage), hit.point);
					}
				} else {
					fxManager.RayCastBullet (projectileOrigin.position, projectileOrigin.position + direction * distance, effectType);
				}
			}
			break;
		case EffectType.RifleBullet:
		case EffectType.HornetBullet:
			spreadVector = new Vector3 (Random.Range (-spread, spread), Random.Range (-spread, spread), Random.Range (-spread, spread)).normalized * Random.Range (0, spread);
			direction += spreadVector;
			direction.Normalize ();

			if (Physics.Raycast (projectileOrigin.position, direction, out hit, distance, shotLayermask.value)) {
				fxManager.RayCastBullet (projectileOrigin.position, hit.point, effectType);

				if (hit.transform.GetComponent<Rigidbody> () != null) {
					hit.transform.GetComponent<Rigidbody> ().AddForce (direction * 5, ForceMode.Impulse);
				}

				if (hit.transform.GetComponent<HitBox> () != null) {
					hit.transform.GetComponent<HitBox> ().DoDamage (Random.Range (minDamage, maxDamage), hit.point);
				}
			} else {
				fxManager.RayCastBullet (projectileOrigin.position, projectileOrigin.position + direction * distance, effectType);
			}
			break;
		}
		currentAmmo--;
		shotTimer = shotDelay;
		animator.SetTrigger ("Shoot");

		if (shootClip != null) {
			gunAudioSource.clip = shootClip;
			gunAudioSource.pitch = Random.Range (shootPitchRange.x, shootPitchRange.y);
			gunAudioSource.Play ();
		}

		if (currentAmmo == 0 || Random.Range (0f, 1f) < 0.03f) {
			charAudio.PlayReload (1f);
			Invoke ("PlayReloadSound", 0.5f);
			reloadTimer = reloadTime;
			currentAmmo = maxAmmo;
		}
	}

	void PlayReloadSound()
	{
		gunAudioSource.clip = reloadClip;
		gunAudioSource.pitch = 1f;
		gunAudioSource.Play ();
	}

	public void SortPlaces (List<Vector3> places, Vector3 sortBy, bool transforms)
	{
		for (int i = 0; i < places.Count - 1; i++) {
			for (int j = 0; j < places.Count - i - 1; j++) {
				if (Vector3.Distance (sortBy, places [j + 1]) < Vector3.Distance (sortBy, places [j])) {
					Vector3 tmp = places [j + 1];
					places [j + 1] = places [j];
					places [j] = tmp;
					if (transforms) {
						Transform t = coverTransforms [j + 1];
						coverTransforms [j + 1] = coverTransforms [j];
						coverTransforms [j] = t;
					}
				}
			}
		}
	}

	public void GetEnemies ()
	{
		if (isFriend) {
			Enemy[] e = FindObjectsOfType<Enemy> ();
			enemies = new Transform[e.Length];
			for (int i = 0; i < e.Length; i++) {
				enemies [i] = e [i].transform;
			}
			Friend[] f = FindObjectsOfType<Friend> ();
			friends = new BotAI[f.Length];
			for (int i = 0; i < f.Length; i++) {
				friends [i] = e [i].transform.GetComponent<BotAI> ();
			}
		} else {
			Friend[] f = FindObjectsOfType<Friend> ();
			enemies = new Transform[f.Length];
			for (int i = 0; i < f.Length; i++) {
				enemies [i] = f [i].transform;
			}
			Enemy[] e = FindObjectsOfType<Enemy> ();
			friends = new BotAI[e.Length];
			for (int i = 0; i < e.Length; i++) {
				friends [i] = e [i].transform.GetComponent<BotAI> ();
			}
		}
	}

	void GetClosestVisibleTarget ()
	{
		RaycastHit hit;
		for (int i = 0; i < enemies.Length; i++) {
			Ray r = new Ray (eyes.position, enemies [i].position - eyes.position + 0.7f *  Vector3.up);
			if (Physics.Raycast (r, out hit, viewDistance, layermask.value)) {
				if (hit.transform.Equals (enemies [i]) || hit.transform.parent == (enemies [i])) {
					if (Vector3.Angle (transform.forward, enemies [i].position - eyes.position) < fov) {
						enemyIndex = i;
						Debug.DrawLine (eyes.position, hit.point, Color.red);
						return;
					} else {
						Debug.DrawLine (eyes.position, hit.point, Color.yellow);
					}
				} else {
					Debug.DrawLine (eyes.position, hit.point, Color.green);
				}
			} else {
				Debug.DrawLine (eyes.position, eyes.position + enemies [i].position - eyes.position, Color.blue);
			}
		}
		enemyIndex = -1;
	}

	void StartPatroling ()
	{
		state = AIState.Patrol;
		SortPlaces (pointsOfInterest, transform.position, false);
		target = pointsOfInterest [Random.Range (1, pointsOfInterest.Count / 2)];
		agent.speed = patrolSpeed;
		agent.destination = target;
	}

	public void ChangeCover ()
	{
		if (enemyIndex >= 0) {
			if (Random.Range (0f, 1f) < 0.5f) {
				return;
			}
			SortPlaces (covers, enemies [enemyIndex].position, true);
			charAudio.PlayTakingFire (0.1f);
		} else {
			SortPlaces (covers, transform.position, true);
			charAudio.PlayTakingFire (0.25f);
		}

		currentCover = Random.Range (0, (int)covers.Count / 2);
		target = covers [currentCover];
		agent.speed = coveringSpeed;
		if (agent.isOnNavMesh) {
			agent.destination = target;
		}
		state = AIState.ChangingCover;
	}

	public void UpdateEnemies ()
	{
		GetEnemies ();
		foreach (Transform t in enemies) {
			if (t.gameObject != gameObject) {
				if (t.GetComponent<BotAI> () != null) {
					t.GetComponent<BotAI> ().GetEnemies ();
				}
			}
		}
	}

	public void AlertFriends (float distance)
	{
		for (int i = 0; i < friends.Length; i++) {
			if (friends [i] != null) {
				if (friends [i].gameObject != gameObject && Vector3.Distance (transform.position, friends [i].transform.position) < distance) {
					friends [i].ChangeCover ();
				}
			}
		}
	}
}
