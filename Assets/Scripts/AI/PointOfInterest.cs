﻿using UnityEngine;
using System.Collections;

public class PointOfInterest : MonoBehaviour 
{
	public Vector3 GetPosition()
	{
		NavMeshHit hit;
		NavMesh.SamplePosition (transform.position, out hit, 5f, NavMesh.AllAreas);
		return hit.position;
	}
}
