﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	public GameObject[] entities;
	public Vector2 intervalBounds;
	public Transform[] spawnPoints;
	private float timer;
	public bool spawning;
	private Transform point;

	public void StartSpawning()
	{
		spawning = true;
		Spawn ();
	}

	public void StopSpawning()
	{
		spawning = false;
	}

	void Spawn()
	{
		point = spawnPoints [Random.Range (0, spawnPoints.Length)];
		Instantiate (entities [Random.Range (0, entities.Length)], point.position, point.rotation);
		timer = Random.Range (intervalBounds.x, intervalBounds.y);
	}

	void Update()
	{
		if (spawning) {
			timer -= Time.deltaTime;
			if (timer < 0) {
				Spawn ();
			}
		}
	}

}
