﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	public int index;
	public GameObject tutorialPanel;
	private Text tutorialText;

	void Awake ()
	{
		tutorialText = tutorialPanel.GetComponentInChildren<Text> ();
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			switch (index) {
			case 0:
				tutorialText.text = "";
				tutorialPanel.SetActive (false);
				break;
			case 1:
				tutorialText.text = "Use movement keys to move\n(default W A S D)\nand sprint key to sprint\n(default shift)";
				tutorialPanel.SetActive (true);
				break;
			case 2:
				tutorialText.text = "Use mouse to look around";
				tutorialPanel.SetActive (true);
				break;
			case 3:
				tutorialText.text = "Use the switch key (default Q)\nto switch between weapons you have";
				tutorialPanel.SetActive (true);
				break;
			case 4:
				tutorialText.text = "Use primary fire button\n(default left mouse button)\nto shoot";
				tutorialPanel.SetActive (true);
				break;
			case 5:
				tutorialText.text = "Use secondary fire button\n(default right mouse button)\nto use special mechanic of your gun";
				tutorialPanel.SetActive (true);
				break;
			case 6:
				tutorialText.text = "Special mechanic can be for example:\nzooming, burst fire, projectile\nnot all guns have special mechanic";
				tutorialPanel.SetActive (true);
				break;
			case 7:
				tutorialText.text = "Use reload key (default R) to reload";
				tutorialPanel.SetActive (true);
				break;
			case 8:
				tutorialText.text = "Use jump key (default spacebar) to jump";
				tutorialPanel.SetActive (true);
				break;
			case 9:
				tutorialText.text = "Point at gun and press change key\n(default E) to pick up the gun";
				tutorialPanel.SetActive (true);
				break;
			case 10:
				tutorialText.text = "Use crouch key (default ctrl) to crouch";
				tutorialPanel.SetActive (true);
				break;
			}
			Destroy (gameObject);
		}

	}
}
