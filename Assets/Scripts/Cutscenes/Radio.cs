﻿using UnityEngine;
using System.Collections;

public class Radio : MonoBehaviour 
{
	public AudioSource source;

	public void Play(AudioClip clip){
		source.clip = clip;
		source.Play ();
	}
}
