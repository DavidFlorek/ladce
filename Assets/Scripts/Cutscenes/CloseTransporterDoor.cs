﻿using UnityEngine;
using System.Collections;

public class CloseTransporterDoor : MonoBehaviour 
{
	public GameObject col;
	public ParticleSystem snow;
	public Animator anim;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			GetComponentInParent<Transporter> ().CloseDoor ();
			col.SetActive (true);
			snow.Stop ();
			anim.SetTrigger ("Play");
			FindObjectOfType<QuitApplication> ().QuitToMenu (10);
			Destroy (gameObject);
		}
	}
}
