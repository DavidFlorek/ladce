﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Sequence
{
	public string text;
	public float duration, delay;
}

public class SubtitleSequence : MonoBehaviour 
{
	private Subtitles sub;

	public Sequence[] subtitles;
	private int currentSub = 0;

	void Awake()
	{
		sub = FindObjectOfType<Subtitles> ();
	}


	void ShowSubtitles()
	{
		sub.ShowSubtitle (subtitles [currentSub].text, subtitles [currentSub].duration);
		currentSub++;
		if (currentSub < subtitles.Length) {
			Invoke ("ShowSubtitles", subtitles [currentSub].delay);
		} else {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			Invoke ("ShowSubtitles", subtitles [0].delay);
			Destroy (GetComponent<BoxCollider> ());
		}
	}
}
