﻿using UnityEngine;
using System.Collections;

public class Transporter : MonoBehaviour 
{
	public Animator transporter, door;
	public GameObject[] collidersToDestroy;
	public AudioSource doorAudio;
	public AudioClip idle, drive;

	private AudioSource transporterAudio;

	void Start()
	{
		transporterAudio = GetComponent<AudioSource> ();
	}

	public void StartTransporter(float time)
	{
		Invoke ("TransporterAnimation", time);
	}

	void TransporterAnimation()
	{
		transporterAudio.clip = drive;
		transporterAudio.Play ();
		transporter.SetTrigger ("Start");
	}

	public void OpenDoor(){
		door.SetTrigger ("Open");
		for(int i = 0; i < collidersToDestroy.Length; i++){
			Destroy(collidersToDestroy[i].gameObject);
		}
		doorAudio.Play ();
		transporterAudio.clip = idle;
		transporterAudio.Play ();
	}

	public void CloseDoor()
	{
		doorAudio.Play ();
		transporterAudio.clip = drive;
		transporterAudio.Play ();
		door.SetTrigger ("Close");
	}
}
