﻿using UnityEngine;
using System.Collections;

public class OpenBayGate : MonoBehaviour 
{
	public Animator doorAnim;
	public GameObject doorPanel;
	public Material mat;
	public Animator gateAnim;
	public Spawner spawner;
	public Transporter transporter;
	public AudioSource doorAudio;
	public AudioSource gateAudio;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			doorAnim.SetTrigger ("Close");
			gateAnim.SetTrigger ("Open");
			MeshRenderer mr = doorPanel.GetComponent<MeshRenderer> ();
			Material[] m = mr.materials;
			m [1] = mat;
			mr.materials = m;
			spawner.StartSpawning ();
			transporter.StartTransporter (60);
			doorAudio.Play ();
			gateAudio.Play ();
			FindObjectOfType<Music> ().PlayInvaders ();
			Destroy (gameObject);
		}
	}
}
