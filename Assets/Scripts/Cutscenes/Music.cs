﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour 
{
	public AudioSource source;
	public AudioClip invadersDrop, invadersOutro;

	public void PlayInvaders()
	{
		StartCoroutine (InvadersCoroutine());
	}

	IEnumerator InvadersCoroutine()
	{
		source.clip = invadersDrop;
		source.Play ();
		yield return new WaitForSeconds (invadersDrop.length);
		source.clip = invadersOutro;
		source.Play ();
	}
}
