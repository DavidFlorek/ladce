﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Subtitles : MonoBehaviour 
{
	public Text subtitleText;

	private float timer;

	void Update()
	{
		if (subtitleText.enabled) {
			timer -= Time.deltaTime;
			if (timer < 0) {
				subtitleText.enabled = false;
			}
		}
	}

	public void ShowSubtitle(string text, float duration){
		subtitleText.enabled = true;
		subtitleText.text = text;
		timer = duration;
	}

}
