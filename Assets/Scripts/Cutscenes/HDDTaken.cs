﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HDDTaken : MonoBehaviour 
{	
	public GameObject hdd;
	public Animator doorAnim;
	public GameObject doorPanel;
	public Material mat;

	public AudioSource doorAudio;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			Destroy (hdd);
			doorAnim.SetTrigger ("Open");
			MeshRenderer mr = doorPanel.GetComponent<MeshRenderer> ();
			Material[] m = mr.materials;
			m [1] = mat;
			mr.materials = m;
			doorAudio.Play ();
			Destroy (gameObject);
		}
	}
}
