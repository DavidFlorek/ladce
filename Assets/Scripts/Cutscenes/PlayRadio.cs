﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayRadio : MonoBehaviour 
{
	public AudioClip clip;
	public GameObject objectivePanel;
	public Text objText;
	public string objectiveText;
	public string subtitleText;

	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player")) {
			FindObjectOfType<Radio> ().Play (clip);
			if (objectiveText != "") {
				objText.text = objectiveText;
				objectivePanel.SetActive (true);
			}
			Destroy (gameObject);
		}
	}
}
